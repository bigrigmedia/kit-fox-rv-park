<?php get_template_part('templates/page', 'header'); ?>

<div class="c-alert c-alert--error">
    <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
</div>
