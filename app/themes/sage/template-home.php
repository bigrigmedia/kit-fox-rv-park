<?php
/**
* Template Name: Home
*/
get_template_part('templates/home-welcome');
get_template_part('templates/home-amenities');
get_template_part('templates/home-activities');
get_template_part('templates/home-gallery');
