<?php
//Set Variables
$contact = get_field('contact_field', 2, true);
$sponsor = get_field('sponsor_field', 2, true);
?>

<footer class="c-footer">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--3@xs c-footer__menu">
                <h5>Kit Fox RV Park</h5>
                <nav class="c-footer__menu">
                    <?php
                    if (has_nav_menu('footer_navigation')) :
                        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'c-nav__list']);
                    endif;?>
                </nav>
            </div>
            <div class="o-col o-col--3@xs c-footer__contact">
                <h5>CONNECT WITH US</h5>
                <?php echo $contact ;?>
            </div>
            <div class="o-col o-col--6@xs c-footer__iframe">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3166.6724235002025!2d-121.17764228468522!3d37.46845557981636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8091b4095da4f6db%3A0x574757591171adbf!2s14750+Rogers+Rd%2C+Patterson%2C+CA+95363!5e0!3m2!1sen!2sus!4v1491342651198" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="c-footer__sponsor">
            <hr />
            <?php echo $sponsor ;?>
            <hr />
        </div>
        <div class="c-footer__rights">
            <p>©2017 KIT FOX RV PARK | RV Park Website Design: <a href="http://www.bigrigmedia.com/">Big Rig Media LLC ®</a></p>
        </div>
    </div>
</footer>
