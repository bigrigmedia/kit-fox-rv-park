<?php
//Variables
$phoneicon = get_field('phone_icon', 2, true);
$phonenumber = get_field('phone_number', 2, true);
?>
<header class="c-header">
    <button class="c-header__toggle c-header__toggle--htx"><span></span></button>
<div class="c-header__mobile">
    <nav class="c-header__menu">
        <?php
        if (has_nav_menu('mobile_navigation')) :
            wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'c-nav__list']);
        endif;?>
    </nav>
    <a href="tel:<?php echo $phonenumber ;?>" class="c-header__number"><img src="<?php echo $phoneicon ;?>"><?php echo $phonenumber ;?></a>
    <a href="/reservations/" class="c-btn c-btn--medium">RESERVE NOW</a>
</div>
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--4@md c-header__phone">
            <a href="tel:<?php echo $phonenumber ;?>"><img src="<?php echo $phoneicon ;?>"><?php echo $phonenumber ;?></a>
            </div>
            <div class="o-col o-col--4@md c-header__logo">
                <a  href="<?= esc_url(home_url('/')); ?>"><img class="c-logo" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt=""/></a>
            </div>
            <div class="o-col o-col--4@md c-header__reserve">
                <a class="c-btn c-btn--medium" href="/reservations/">RESERVE NOW</a>
            </div>
        </div>
        <hr />
        <nav class="c-nav">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'c-nav__list']);
            endif;
            ?>
        </nav>
    </div>
</header>
