<?php
//Set Variables

$activity = get_field('activites_field', 2, true);
?>
<div class="o-content c-activites">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-activites__content">
                <?php echo $activity ;?>
            </div>
        </div>
    </div><!-- o-row -->
</div><!-- o-container -->
</div><!-- c-about-->
