<?php
//Set Variables
$amenities = get_field('amenities_field', 2, true);
?>
<div class="o-content c-amenities">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-amenities__content">
                <?php echo $amenities;?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
