<?php
//Set Variables
$discount = get_field('discount_field', 2, true);
?>
<div class="o-content c-discount">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-discount__content">
                <?php echo $discount;?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
