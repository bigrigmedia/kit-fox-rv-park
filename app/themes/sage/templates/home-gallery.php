<?php
//Set Variables
$gallery = get_field('gallery_field', 2, true);
?>
<div class="o-content c-gallery">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-gallery__content">
                <?php echo $gallery ;?>
            </div>
        </div>
    </div><!-- o-row -->
</div><!-- o-container -->
</div><!-- c-about-->
