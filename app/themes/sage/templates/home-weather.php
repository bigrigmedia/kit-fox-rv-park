
<div class="o-content c-weather">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-weather__content">
                <?php echo do_shortcode('[wunderground location=14750 Rogers Rd, Patterson, CA 95363 numdays=4 layout=simple]');?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
