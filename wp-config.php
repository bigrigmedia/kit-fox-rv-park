<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Disable automatic update
 *
 * @link http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */
define('AUTOMATIC_UPDATER_DISABLED', true);

/**
 * Limit number of revisions saved
 *
 * @link https://codex.wordpress.org/Revisions
 */
define('WP_POST_REVISIONS', 10);

/**
 * Update default mem limit
 *
 * @link http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP
 */
define('WP_MEMORY_LIMIT', '64M');

/**
 * Disable dashboard file editing
 *
 * @link http://codex.wordpress.org/Hardening_WordPress#Disable_File_Editing
 */
define('DISALLOW_FILE_EDIT', true);

/**
 * WordPress Environment
 *
 * The default usage is:
 * 	development - For local development
 *	production - For live site
 */
define('WP_ENV', 'development');

// Switch MySQL settings based on environment
switch(WP_ENV){
	// Development
	case 'development':
		/** The name of the local database for WordPress */
		define('DB_NAME', 'kit-fox-rv_db');

		/** MySQL local database username */
		define('DB_USER', 'root');

		/** MySQL local database password */
		define('DB_PASSWORD', '');
	break;
	// Production
	case 'production':
		/** The name of the live database for WordPress */
		define('DB_NAME', 'live_database_name_here');

		/** MySQL live database username */
		define('DB_USER', 'live_username_here');

		/** MySQL live database password */
		define('DB_PASSWORD', 'live_password_here');
	break;
}

// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Custom Content Directory **/
define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/app');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         'J1xLu9^Fyed7Fw8:RM3Uvajp|}yA|}W8u<J#=AlWUe]H[lApK1|+Tk|i-k2Ed:y*');
 define('SECURE_AUTH_KEY',  'M9g2 qT*}H&z2=d 7nco-CvY0L[Dmu~FLc(Xa7<f{nFbXA@j9m I$zb>+6#;K*Gk');
 define('LOGGED_IN_KEY',    'N+C?|-,ONd.--9(_>+SNfM|^bNRIpC5S.eHl^1(KT4j72tew. :gExif;t6o$|(c');
 define('NONCE_KEY',        'e<@[8+|CvX,*sk3;t|VD O5?mCg`t$E}t|[tpH,Vim#,k/[yh#;uf*>S0l+^HKDe');
 define('AUTH_SALT',        '8K|3dhFG$ (Mii)TrMI{j&Lrp[d6F]xNw5(k*ml#Vv*ikfCi73,Hu,9N%V,/_Xk|');
 define('SECURE_AUTH_SALT', '|<]7+Ir|lcv7IifayM2&@krW-3X_LGi{*!+rjK&hy4b:u;7G#Dj{4a`xKBEX< NQ');
 define('LOGGED_IN_SALT',   'tI-aVjs66XtTRHq8+&OO+yA-]]ClWS$I3UvN~Z{Q[|jyR1SjnQ8@2m-E;0o@`YR}');
 define('NONCE_SALT',       'ND%1}_OE}%.wK1^4/a9pZl?5_Qc;T[hqZEbB$&9?R[EveEU0IX:S hZmq2MG]b+H');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'kp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Enable theme debug mode **/
define('THEME_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/wp/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
